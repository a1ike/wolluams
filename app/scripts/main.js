function hideAllError(str) { return true; }
window.onerror = hideAllError;

$(document).ready(function () {

  $('.phone').inputmask({
    mask: '+7(999)-999-99-99',
    showMaskOnHover: false
  });

  $('.w-features__link').on('click', function (e) {
    e.preventDefault();

    $(this).prev().removeClass('w-features__content_locked');
    $(this).hide();
  });

  $('.w-header__mob-toggle').on('click', function (e) {
    e.preventDefault();

    $('.w-header').toggleClass('w-header_opened');
  });

  $('.w-header__toggle img').on('click', function (e) {
    e.preventDefault();

    $(this).next().slideToggle('fast');
  });

  $('.open-modal').on('click', function (e) {
    e.preventDefault();

    $('.w-modal').toggle();
  });

  $('.w-modal__centered').on('click', function (e) {
    e.preventDefault();

    if (e.target.className === 'w-modal__centered') {
      $('.w-modal').hide();
    }
  });

  $('.w-modal__close').on('click', function (e) {
    e.preventDefault();
    $('.w-modal').hide();
  });

  $('.w-aside__dropper').on('click', function (e) {
    e.preventDefault();

    $(this).children().next().slideToggle('fast');
  });

  $('.w-aside__title').on('click', function (e) {
    e.preventDefault();

    $(this).toggleClass('w-aside__title_active');
    $(this).next().slideToggle('fast');
  });

  $('.w-tabs__header li').click(function () {
    var tab_id = $(this).attr('data-tab');

    $('.w-tabs__header li').removeClass('current');
    $('.w-tabs__content').removeClass('current');

    $(this).addClass('current');
    $('#' + tab_id).addClass('current');
  });

  $('.w-basket-card__minus').on('click', function (e) {
    e.preventDefault();

    var currentVal = $(this).next().val();

    if (currentVal > 1) {
      $(this).next().val(--currentVal);
    }
  });

  $('.w-basket-card__plus').on('click', function (e) {
    e.preventDefault();

    var currentVal = $(this).prev().val();

    if (currentVal < 99) {
      $(this).prev().val(++currentVal);
    }
  });

  $(window).on('load resize scroll', function (e) {

    if ($(window).width() > 1199) {

      window.onscroll = function () { scrollFunction() };

      function scrollFunction() {
        if (document.documentElement.scrollTop > 200) {
          $('.w-header__fav').detach().appendTo('.w-fixed-help');
          $('.w-header__basket').detach().appendTo('.w-fixed-help');
          $('.w-header__main').hide();
        } else {
          $('.w-header__fav').detach().appendTo('.w-header__right');
          $('.w-header__basket').detach().appendTo('.w-header__right');
          $('.w-header__main').show();
        }
      }
    }
  });

  new Swiper('.w-welcome', {
    pagination: {
      el: '.w-welcome .swiper-pagination',
      clickable: true,
    }, autoplay: {
      delay: 5000,
    },
  });

  new Swiper('#woman .w-cards__cards', {
    navigation: {
      nextEl: '#woman .swiper-button-next',
      prevEl: '#woman .swiper-button-prev',
    },
    spaceBetween: 40,
    breakpoints: {
      768: {
        slidesPerView: 2,
      },
      1024: {
        slidesPerView: 3,
      },
      1200: {
        slidesPerView: 4,
      },
    }
  });

  new Swiper('#men .w-cards__cards', {
    navigation: {
      nextEl: '#men .swiper-button-next',
      prevEl: '#men .swiper-button-prev',
    },
    spaceBetween: 40,
    breakpoints: {
      768: {
        slidesPerView: 2,
      },
      1024: {
        slidesPerView: 3,
      },
      1200: {
        slidesPerView: 4,
      },
    }
  });

  new Swiper('#child .w-cards__cards', {
    navigation: {
      nextEl: '#child .swiper-button-next',
      prevEl: '#child .swiper-button-prev',
    },
    spaceBetween: 40,
    breakpoints: {
      768: {
        slidesPerView: 2,
      },
      1024: {
        slidesPerView: 3,
      },
      1200: {
        slidesPerView: 4,
      },
    }
  });

  new Swiper('#toys .w-cards__cards', {
    navigation: {
      nextEl: '#toys .swiper-button-next',
      prevEl: '#toys .swiper-button-prev',
    },
    spaceBetween: 40,
    breakpoints: {
      768: {
        slidesPerView: 2,
      },
      1024: {
        slidesPerView: 3,
      },
      1200: {
        slidesPerView: 4,
      },
    }
  });

  new Swiper('.w-good__images', {
    navigation: {
      nextEl: '.w-good__images .swiper-button-next',
      prevEl: '.w-good__images .swiper-button-prev',
    },
    pagination: {
      el: '.w-good__images .swiper-pagination',
      clickable: true,
    },
    breakpoints: {
      1200: {
        slidesPerView: 2,
      },
    }
  });

  $('#rating').barrating({
    theme: 'fontawesome-stars-o',
    showSelectedRating: false
  });

  var slider = document.getElementById('price');

  noUiSlider.create(slider, {
    start: [99, 20000],
    connect: true,
    range: {
      'min': 0,
      'max': 50000
    }
  });

  slider.noUiSlider.on('update', function (values, handle) {

    $('#min').val(Math.round(values[0]));
    $('#max').val(Math.round(values[1]));
  });

  $('#min').on('input', function () {
    slider.noUiSlider.set([$(this).val(), null]);
  });

  $('#max').on('input', function () {
    slider.noUiSlider.set([null, $(this).val()]);
  });
});